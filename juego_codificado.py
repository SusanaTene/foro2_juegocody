import pygame
from random import random
from pygame.locals import *

"""En esta parte le cambie resolucion de la pantalla de pequeña a mas grande y visible"""
resolution = (800, 600)

red = (255,0,0)
white = (255, 255, 255)
black = (0, 0, 0)

screen = pygame.display.set_mode(resolution)
""""En esta parte le agregue un código para cambiar el titulo de juego"""
pygame.display.set_caption("Salva a Cody esquivanado las bombas ")

""""en esta parte renombre la clase"""
class Image:
    def __init__(self, xPos=resolution[0] / 2, yPos=resolution[1] / 2, xVel=1, yVel=1,rad=15):
        self.x = xPos
        self.y = yPos
        self.dx = xVel
        self.dy = yVel
        self.radius = rad
        self.type = "image"

    """"Aquí agregue un código para agregar imagen"""
    def image(self, surface):
        fondo = pygame.image.load("resources/bom.png")
        screen.blit(fondo, (self.x,self.y))

    def draw(self, surface):
        pygame.draw.circle(surface, black, (self.x, self.y), self.radius)

    def update(self, gameObjects):
        self.x += self.dx
        self.y += self.dy
        if (self.x <= 0 or self.x >= resolution[0]):
            self.dx *= -1
        if (self.y <= 0 or self.y >= resolution[1]):
            self.dy *= -1
class Player:
    def __init__(self, rad=20):
        self.x = 0
        self.y = 0
        self.radius = rad
        self.type = "player"

    """"Aquí agregue un código para agregar imagen"""
    def image(self, surface):
        fondo = pygame.image.load("resources/tux.png")
        screen.blit(fondo, (self.x,self.y))

    def draw(self, surface):
        pygame.draw.circle(surface, red, (self.x, self.y), self.radius)

    def update(self, gameObjects):
        cord = pygame.mouse.get_pos()
        self.x = cord[0]
        self.y = cord[1]
        for gameObj in gameObjects:
            if gameObj.type == "image":
                if (gameObj.x - self.x) ** 2 + (gameObj.y - self.y) ** 2 <= (gameObj.radius + self.radius) ** 2:
                    pygame.quit()
class GameController:
    def __init__(self, interval = 5):
        self.inter = interval
        self.next = pygame.time.get_ticks() + (2 * 1000)
        self.type = "game controller"

    """este código lo comente porque cuando se chocan los objetos se me queda la pantala abierta """
    #self.score = 0
    #self.scoreText = pygame.font.Font(None, 12)

    def update(self, gameObjects):
        if self.next < pygame.time.get_ticks():
            self.next = pygame.time.get_ticks() + (self.inter * 1000)
            gameObjects.append(Image(xVel=random()*2, yVel=random()*2))

    """"Aquí cambie  la funcion dibujar por la imagen que se sube anteriormente"""
    def image(self, screen):
        pass


class game():
    def __init__(self):
        pygame.init()

        self.screen = pygame.display.set_mode(resolution)
        self.clock = pygame.time.Clock()
        self.gameObjects = []
        self.gameObjects.append(GameController())
        self.gameObjects.append(Player())


    def handleEvents(self):
        for event in pygame.event.get():
            if event.type == QUIT:
               pygame.quit()

    """"Aquí cambie el if por un for para la ejecución de los eventos """
    def run(self):
        while True:
            self.handleEvents()
            for gameObj in self.gameObjects:
                gameObj.update(self.gameObjects)

            self.screen.fill(white)

            for gameObj in self.gameObjects:
                gameObj.image(self.screen)

            self.clock.tick(60)
            pygame.display.flip()


game().run()
